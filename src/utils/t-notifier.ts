import { Notify } from 'quasar'

interface Notifier {
  message : any;
  color? : string;
  position? : "top" | "top-left" | "top-right" | "bottom-left" | "bottom-right" | "bottom"
}

export default function notifier ({ message, color, position }: Notifier) {
  Notify.create({
    message,
    color: color || 'dark',
    position: position || 'top'
  })
}
