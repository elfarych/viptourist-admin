import { LocalizationInterface } from 'src/models/TourModel'
import { i18n } from 'boot/i18n'
import notifier from '../utils/t-notifier'
import OfferInterface from 'src/models/Offer'
import axios from 'axios'
import server from 'src/config/server'
import errorHandler from 'src/utils/errorHandler'

export default class Offer {
  id: string;
  languages: any;
  prerequisites: any;
  prohibitions: any;
  included: any;
  createdLanguage :string;
  // eslint-disable-next-line camelcase
  not_included: any;
  note: any;
  localizations: [LocalizationInterface];
  translationApproved: boolean;

  constructor (offer: OfferInterface) {
    this.id = offer.id
    this.languages = offer.languages?.split('|')
    this.prerequisites = offer.prerequisites?.split('|')
    this.prohibitions = offer.prohibitions?.split('|')
    this.included = offer.included?.split('|')
    this.not_included = offer.not_included?.split('|')
    this.note = offer.note
    this.localizations = offer.localizations
    this.createdLanguage = offer.createdLanguage
    this.translationApproved = offer.translationApproved
  }

  getStringValues (): any {
    return {
      languages: this.arrayTooString(this.languages),
      prerequisites: this.arrayTooString(this.prerequisites),
      prohibitions: this.arrayTooString(this.prohibitions),
      included: this.arrayTooString(this.included),
      not_included: this.arrayTooString(this.not_included),
      note: this.note || ''
    }
  }

  arrayTooString (item: []) : string {
    return item.join('|') || ''
  }

  async update (): Promise<void> {
    const vm = this
    try {
      await axios
        .put(`${server.serverURI}/offers/${vm.id}`, {
          ...vm.getStringValues(),
          translationApproved: this.translationApproved
        })
        .then(response => {
          notifier({ message: i18n.t('offerUpdated'), color: 'positive' })
        })
    } catch (err) {
      errorHandler(err)
    }
  }
}
