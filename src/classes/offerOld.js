import { updateOriginalModel } from 'src/utils/modelActions'
import { mapMutations } from 'vuex'
import notifier from 'src/utils/notifier'
import { i18n } from 'boot/i18n'

export default class Offer {
  constructor (offer) {
    this.languages = offer.languages?.split(',')
    this.prerequisites = offer.prerequisites?.split(',')
    this.prohibitions = offer.prohibitions?.split(',')
    this.included = offer.included?.split(',')
    this.not_included = offer.not_included?.split(',')
    this.note = offer.note ? [offer.note] : ''
  }

  getStringValues () {
    return {
      languages: this.languages?.join(','),
      prerequisites: this.prerequisites?.join(',') || '',
      prohibitions: this.prohibitions?.join(',') || '',
      included: this.included?.join(',') || '',
      not_included: this.not_included?.join(',') || '',
      note: this.note[0] || ''
    }
  }

  async updateOriginal (vm) {
    return updateOriginalModel({
      modelName: 'offers',
      callback: () => mapMutations('offers', ['mutationOriginalOffer']),
      data: this.getStringValues(),
      vm
    })
      .then(() => notifier({ message: i18n.t('offerUpdated'), color: 'positive' }))
  }
}
