import { TourInterface, LocalizationInterface } from 'src/models/TourModel'
import axios from 'axios'
import server from 'src/config/server'
import errorHandler from 'src/utils/errorHandler'
import notifier from 'src/utils/t-notifier'
import { i18n } from 'boot/i18n'

class Tour {
  id: string;
  name: string;
  description: string;
  translationApproved: boolean;
  locale: string;
  createdLanguage :string;
  original: TourInterface | undefined;
  localizations: [LocalizationInterface]
  /**
   * Creates an instance of tour.
   */
  constructor (tour: TourInterface) {
    this.id = tour.id
    this.name = tour.name
    this.description = tour.description
    this.locale = tour.locale
    this.translationApproved = tour.translationApproved
    this.createdLanguage = tour.createdLanguage
    this.localizations = tour.localizations
  }

  async loadOriginalTour (): Promise<void> {
    const locale: LocalizationInterface | undefined = this.localizations.find(item => item.locale === this.createdLanguage)
    const id: string | undefined = locale?.id

    try {
      this.original = await axios
        .get(`${server.serverURI}/tours/${id}`, {
          headers: server.headers
        })
        .then(response => {
          return response.data
        })
    } catch (err) {
      errorHandler(err)
    }
  }

  /**
   * Updates tour
   * @param tour
   * @returns tour
   */
  async update (): Promise<void> {
    try {
      await axios
        .put(`${server.serverURI}/tours/${this.id}`, {
          name: this.name,
          translationApproved: this.translationApproved,
          description: this.description
        })
        .then(response => {
          notifier({ message: i18n.t('tourUpdated'), color: 'positive' })
        })
    } catch (err) {
      errorHandler(err)
    }
  }
}

export default Tour
