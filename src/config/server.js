export default {
  // serverURI: 'http://192.168.0.199:1337',
  serverURI: 'http://37.140.241.144:1337',
  // notificationServerURI: 'http://192.168.0.199:1338',
  notificationServerURI: 'http://37.140.241.144:1338',
  headers: {
    Authorization: `Bearer ${localStorage.getItem('jwt')}`
  },

  setHeaders (jwt) {
    this.headers.Authorization = `Bearer ${jwt}`
  }
}
