export default {
  computed: {
    links () {
      return [
        { routeName: 'tours', title: this.$t('Tours'), icon: 'las la-map-marked' },
        { routeName: 'offers', title: this.$t('Offers'), icon: 'las la-tags' },
        { routeName: 'notifications', title: this.$t('notifications'), icon: 'las la-comment' }
        // { routeName: 'countries', title: this.$t('Countries'), icon: 'las la-globe' },
        // { routeName: 'cities', title: this.$t('Cities'), icon: 'las la-city' }
      ]
    }
  }
}
