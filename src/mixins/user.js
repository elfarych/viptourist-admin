import { mapState } from 'vuex'

export default {
  computed: {
    ...mapState('user', ['user']),
    language () {
      return localStorage.getItem('language')
    }
  },

  watch: {
    user (val) {
      console.log('user ', val)
    },
    language (val) {
      console.log('lang ', val)
    }
  }
}
