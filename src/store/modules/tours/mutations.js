export function mutationTours (state, data) {
  state.tours = data
}

export function mutationOriginalTour (state, data) {
  state.originalTour = data
}

export function mutationTour (state, data) {
  state.tour = data
}
