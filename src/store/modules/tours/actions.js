import axios from 'axios'
import errorHandler from 'src/utils/errorHandler'
import server from 'src/config/server'
import notifier from 'src/utils/notifier'
import { i18n } from 'boot/i18n'

export async function loadTours ({ commit, dispatch }) {
  try {
    await axios
      .get(`${server.serverURI}/tours`, {
        params: {
          _locale: localStorage.getItem('language')
        }
      })
      .then(response => {
        commit('mutationTours', response.data)
      })
  } catch (err) {
    errorHandler(err)
  }
}

export async function loadOriginalTour ({ commit }) {
  const vm = this
  try {
    await axios
      .get(`${server.serverURI}/tours/${vm.$router.currentRoute.params.id}`, {
        headers: server.headers
      })
      .then(response => {
        return commit('mutationOriginalTour', response.data)
      })
  } catch (err) {
    errorHandler(err)
  }
}

export async function updateTour ({ commit }, data) {
  try {
    await axios
      .put(`${server.serverURI}/tours/${data.id}`, {
        ...data
      }, {
        headers: server.headers
      })
      .then(response => {
        commit('mutationTour', response.data)
        return notifier({ message: i18n.t('tourUpdated'), color: 'positive' })
      })
  } catch (err) {
    errorHandler(err)
  }
}

export async function loadTour ({ commit }) {
  const id = this.$router.currentRoute.params.id

  try {
    await axios
      .get(`${server.serverURI}/tours/${id}`,
        {
          headers: server.headers
        })
      .then(response => {
        commit('mutationTour', response.data)
      })
  } catch (err) {
    errorHandler(err)
  }
}
