import axios from 'axios'
import errorHandler from 'src/utils/errorHandler'
import server from 'src/config/server'
import notifier from 'src/utils/notifier'
import { i18n } from 'boot/i18n'

export async function loginUser ({ commit, dispatch }, payload) {
  try {
    await axios
      .post(`${server.serverURI}/auth/local`, {
        identifier: payload.email,
        password: payload.password
      })
      .then(response => {
        commit('mutationsUser', response.data.user)
        server.setHeaders(response.data.jwt)
        localStorage.setItem('jwt', response.data.jwt)
        localStorage.setItem('language', response.data.user.language.code)
        i18n.locale = localStorage.getItem('language') || 'ru-RU'
        return notifier({ message: i18n.t('SuccessAuthorization'), color: 'positive' })
      })
      .catch(e => notifier({ message: e.message, color: 'negative' }))
  } catch (err) {
    errorHandler(err)
  }
}

export async function getUser ({ commit }, jwt) {
  try {
    await axios
      .get(`${server.serverURI}/users/me`, {
        headers: {
          Authorization: `Bearer ${jwt}`
        }
      })
      .then(response => {
        server.setHeaders(jwt)
        commit('mutationsUser', response.data)
        i18n.locale = localStorage.getItem('language') || 'ru-RU'
        return notifier({ message: i18n.t('SuccessLogged'), color: 'positive' })
      })
  } catch (err) {
    errorHandler(err)
  }
}
