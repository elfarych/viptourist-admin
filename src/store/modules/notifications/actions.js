import axios from 'axios'
import errorHandler from 'src/utils/errorHandler'
import server from 'src/config/server'
import { i18n } from 'boot/i18n'
import notifier from 'src/utils/t-notifier'

export async function loadNotifications ({ commit }) {
  let allNotifications = []
  try {
    const notifications = await axios
      .get(`${server.notificationServerURI}/general-notifications`, {
        params: {
          _locale: localStorage.getItem('language')
        }
      })
      .then(response => response.data)

    const messages = await axios
      .get(`${server.notificationServerURI}/messages`, {
        params: {
          _locale: localStorage.getItem('language')
        }
      })
      .then(response => response.data)
    allNotifications = [...notifications, ...messages]
    commit('mutationNotifications', allNotifications)
  } catch (err) {
    errorHandler(err)
  }
}

export async function loadNotification ({ commit }, id) {
  const translationId = this.$router.currentRoute.params.id || id
  try {
    await axios
      .get(`${server.notificationServerURI}/general-notifications/${translationId}`)
      .then(response => {
        commit('mutationNotification', response.data)
      })
  } catch (err) {
    try {
      await axios
        .get(`${server.notificationServerURI}/messages/${translationId}`)
        .then(response => {
          commit('mutationNotification', response.data)
        })
    } catch (err) {
      errorHandler(err)
    }
  }
}

export async function updateNotification ({ commit }, payload, id) {
  if (!id) {
    id = this.$router.currentRoute.params.id
  }
  try {
    await axios
      .put(`${server.notificationServerURI}/general-notifications/${id}`, {
        ...payload
      })
      .then(response => {
        notifier({ message: i18n.t('notificationUpdatedSuccessfully'), color: 'positive' })

        commit('mutationNotification', response.data)
      })
  } catch (err) {
    try {
      await axios
        .put(`${server.notificationServerURI}/messages/${id}`, {
          ...payload
        })
        .then(response => {
          notifier({ message: i18n.t('notificationUpdatedSuccessfully'), color: 'positive' })
          commit('mutationNotification', response.data)
        })
    } catch (err) {
      errorHandler(err)
    }
  }
}
