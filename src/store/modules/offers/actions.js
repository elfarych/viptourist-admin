import axios from 'axios'
import errorHandler from 'src/utils/errorHandler'
import server from 'src/config/server'

export async function loadOffers ({ commit }) {
  try {
    await axios
      .get(`${server.serverURI}/offers`, {
        params: {
          locale: localStorage.getItem('language'),
          _locale: 'all'
        }
      })
      .then(response => {
        commit('mutationOffers', response.data)
      })
  } catch (err) {
    errorHandler(err)
  }
}

export async function loadOffer ({ commit }) {
  const id = this.$router.currentRoute.params.id
  try {
    await axios
      .get(`${server.serverURI}/offers/${id}`,
        {
          headers: server.headers
        })
      .then(response => {
        commit('mutationOffer', response.data)
      })
  } catch (err) {
    errorHandler(err)
  }
}
