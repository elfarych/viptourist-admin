export function mutationOffers (state, data) {
  state.offers = data
}

export function mutationOriginalOffer (state, data) {
  state.originalOffer = data
}

export function mutationOffer (state, data) {
  state.offer = data
}
