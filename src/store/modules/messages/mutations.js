export function mutationNotifications (state, data) {
  state.notifications = [state.notifications, ...data]
}

export function mutationNotification (state, data) {
  state.notification = data
}
