import axios from 'axios'
import errorHandler from 'src/utils/errorHandler'
import server from 'src/config/server'
import { i18n } from 'boot/i18n'

export async function loadNotifications ({ commit }) {
  try {
    await axios
      .get(`${server.notificationServerURI}/general-notifications`, {
        params: {
          _locale: localStorage.getItem('language')
        }
      })
      .then(response => {
        commit('mutationNotifications', response.data)
      })
  } catch (err) {
    errorHandler(err)
  }
}

export async function loadNotification ({ commit }, id) {
  const translationId = this.$router.currentRoute.params.id || id
  try {
    await axios
      .get(`${server.notificationServerURI}/general-notifications/${translationId}`)
      .then(response => {
        commit('mutationNotification', response.data)
      })
  } catch (err) {
    errorHandler(err)
  }
}

export async function updateNotification ({ commit }, payload, id) {
  if (!id) {
    id = this.$router.currentRoute.params.id
  }
  try {
    await axios
      .put(`${server.notificationServerURI}/general-notifications/${id}`, {
        ...payload
      })
      .then(response => {
        i18n.t('notificationUpdatedSuccessfully')
        commit('mutationNotification', response.data)
      })
  } catch (err) {
    errorHandler(err)
  }
}
