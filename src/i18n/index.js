import enUS from './en'
import ruRU from './ru-RU'
import frFR from './fr-FR'
import trTR from './tr-TR'
import itIT from './it-IT'
import esES from './es-ES'
import deDE from './de-DE'
import thTH from './th-TH'
import arAE from './ar-AE'

export default {
  en: enUS,
  'ru-RU': ruRU,
  'fr-FR': frFR,
  'tr-TR': trTR,
  'it-IT': itIT,
  'es-ES': esES,
  'de-DE': deDE,
  'th-TH': thTH,
  'ar-AE': arAE
}
