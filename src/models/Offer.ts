import { LocalizationInterface } from 'src/models/TourModel'

interface OfferInterface {
  id: string;
  languages?: any;
  prerequisites?: any;
  prohibitions?: any;
  included?: any;
  // eslint-disable-next-line camelcase
  not_included?: any;
  note?: any;
  localizations: [LocalizationInterface];
  createdLanguage :string;
  translationApproved: boolean;
}

export default OfferInterface
